#  _                     _   _                _
# | |                   | | | |              | |
# | |__   ___  __ _ _ __| |_| |__   ___  __ _| |_
# | '_ \ / _ \/ _` | '__| __| '_ \ / _ \/ _` | __|
# | | | |  __/ (_| | |  | |_| |_) |  __/ (_| | |_
# |_| |_|\___|\__,_|_|   \__|_.__/ \___|\__,_|\__|
#
#
#                      _                   _
#                     | |                 | |
#   ___ ___  _ __  ___| |_ _ __ _   _  ___| |_
#  / __/ _ \| '_ \/ __| __| '__| | | |/ __| __|
# | (_| (_) | | | \__ \ |_| |  | |_| | (__| |_
#  \___\___/|_| |_|___/\__|_|   \__,_|\___|\__|

# by zappy
# (python, 2020)


# is this a poem in the form of code?
#               OR
# is this code in the form of a poem?










# CHARM TO SUMMON THE CONSTRUCT
#
# When one runs this code with python3,
# a woman is created, with a heart, by thee.
# It will beat a while, by text instead
# of blood, until that fateful time when she is dead.












            #              .---.
            #             / ,-- \
            #     .--.   ( (^_^) )   .--.
            #   ,'    \  (.-`-'(_)  /    `.
            #  /       `-/ \ `.  \-'       \
            # : (_,' .  / (.\_ ") \  . `._) :
            # |   `-'(_,\ \     / /._)`-'   |
            # |       .  `.\,O,'.'  .   :   |
            # |   . : !   /\_  /\   ! . !   |
            # | ! |-'-|  : ""T"" :  |-'-| | |
            # | |-'   `-'|   H   |`-'   `-| |
            # `-'        |   H .:|        `-'
            #            | . H !||
            #            | : H :!|
            #            | ! H !||
            #            | | H |||
            #            | | H |||  Ojo 98
            #            /_,'V.L|.\











# In that brilliant flash of light where God
# pulled life wholesale from the lone point where all
# dwelled then in harmony, was there a flick
# of her finger, a wild yelling, arms out
# flailing, a dance for summoning the Earth
# and Moon and Stars? Or, did our great Mother
# do it with Eleven little key strokes?

import time










            #       (\                     /)
            #       (v\                   /v)
            #      (vvv\                 /vvv)
            #     (vvvvv\               /vvvvv)
            #    (vvvvvvv\             /vvvvvvv)
            #   (vvvvvvvvv\   _---_   /vvvvvvvvv)
            #  (vvvvvvvvvvv\/  XII  \/vvvvvvvvvvv)
            # (vvvvvvvvvvvv/      /  \vvvvvvvvvvvv)
            # (vvvvvvvvvvv/      /    \vvvvvvvvvvv)
            # (vvvvvvvvvv|IX   @   III |vvvvvvvvvv)
            #  (vvvvvvvvvv\      \    /vvvvvvvvvv)
            #   (vvvvvvvvvv\         /vvvvvvvvvv)
            #     (vvvvvvvvv\  VI   /vvvvvvvvv)
            #        (vvvvvvvv-___-vvvvvvvv)
            #          (vvvvvv/   \vvvvvv)
            #          (vvvvv/     \vvvvv)
            #           (vvv/       \vvv)
            # ejm97      (v/         \v)
            #            (/           \)










# Seven minutes.
# Seven minutes is the average lifespan
# of the woman created herein.
# She could, of course pass at the first
# try through the while loop,
# but on average it will take seven cycles
# for her to perish and leave
# the sordid world she will soon inhabit.

import random















            #        .-------.    ______
            #       /   o   /|   /\     \
            #      /_______/o|  /o \  o  \
            #      | o     | | /   o\_____\
            #      |   o   |o/ \o   /o    /
            #      |     o |/   \ o/  o  /
            # jgs  '-------'     \/____o/












def get_heartbeat ():
    # a list of experiences one imagines
    # the woman might feel, if she could feel anything.
    activity_list = {
        "1":63.6, # sitting on the couch, watching TV.
        "2":92.9, # when she sees her crush at the Safeway.
        "3":57.5, # laying in bed, trying her hardest forget.
        "4":77.7, # watching a stray skitter across the street.
        "5":86.8, # a 1-on-1 meeting invite with her manager.
        "6":49.4, # a lazy summer afteroon lounging.
        "7":00.0  # passing on.
    }
    # in a better world we could choose our memories
    activity = activity_list[str(random.randint(1,7))]
    # but here that task is left to the fates.

    return activity











# Right here is her moment of conception!
# Glory, Glory, Hallelujah!
# Yet, the heart does not now beat.
# Can we truly say, here, that life exists?

alive = True









# maybe that's too divisive a discussion for a python comment.
# maybe we should take this offline?







# Symbols exist not for their own sake
# but in a name,
# facades for a truth yet known,
# effigies to reality beyond.
# Value taken and copied away for later needs.
# Now guarded via pointer that holds traces beckoning to true forms.

arc_inc = 1
arc_n = 0








                    #     %%%
                    #    =====
                    #   &%&%%%&
                    #   %& < <%
                    #    &\__/
                    #     \ |____
                    #    .', ,  ()
                    #   / -.  _)|
                    #  |_(_.    |
                    #  '-'\  )  |
                    #  mrf )    |
                    #     /  .  ).
                    #    /    _. |
                    #  /'---':.-'|
                    # (__.' /    /
                    #  \   ( /  /
                    #   \ /  _  |
                    #    \  |  '|
                    #    | . \  |
                    #    |(     |
                    #    |  \ \ |
                    #     \  )\ |
                    #    __)/ / \
                    # --"--(_.Ooo'----









# Time erodes all that falls in its path, the last
hourglass = 0.0#fractures from the harsh winds, swirling the
sands = 0.0#across the barren wastes. One day the last
heart_rate = 0.0#will drop to zero, leaving blasted
# ruins now inhabited only by ghosts.














                    # +8-=-=-=-=-=-8+
                    #  | ,.-'"'-., |
                    #  |/         \|
                    #  |\:.     .:/|
                    #  | \:::::::/ |
                    #  |  \:::::/  |
                    #  |   \:::/   |
                    #  |    ):(    |
                    #  |   / . \   |
                    #  |  /  .  \  |
                    #  | /   .   \ |
                    #  |/   .:.   \|
                    #  |\.:::::::./|
                    #  | '--___--' |
                    # +8-=-=-=-=-=-8+







while alive:
    if hourglass <= 0.0:
        heart_rate = get_heartbeat()
        if heart_rate > 0.0:
            sands = 60.0 / heart_rate
            hourglass = 60.0
            print("new heart rate : " + str(heart_rate))
        else:
            alive = False
            print('MEMENTO MORI')
            print('Her life was simple, quaint,')
            print('wrought here from aether and')
            print('shown little life but the')
            print('slow beating heart you gave')
            print('to her. She felt no pain,')
            print('no love. Now watch as that')
            print('pale heart expends its last')
            print('and returns back to nothing.')

    hourglass -= sands






    arc = ''
    n = 0
    while n < arc_n:
        arc = arc + '\t'
        n += 1

    arc_n += arc_inc

    if arc_n == 5:
        arc_inc = -1
    elif arc_n == 0:
        arc_inc = 1

    print(arc + 'ba bum')
    time.sleep(sands)





# written by Max West in the year 2020
# free and open source, always
# gitlab : https://gitlab.com/zappymax/heartbeat-construct
# special thanks to https://www.asciiart.eu/

            #                    ,____
            #                    |---.\
            #            ___     |    `
            #           / .-\  ./=)
            #          |  |"|_/\/|
            #          ;  |-;| /_|
            #         / \_| |/ \ |
            #        /      \/\( |
            #        |   /  |` ) |
            #        /   \ _/    |
            #       /--._/  \    |
            #       `/|)    |    /
            #         /     |   |
            #       .'      |   |
            # jgs  /         \  |
            #     (_.-.__.__./  /
# /|/ \|\ /|/ \|\ /|/ \|\ /|/
# /|/ \|\ /|/ \|\ /|/ \|\ /|/
